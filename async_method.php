<?php
include "clases.php";
$obj = new Metodos();

if($_POST['type'] == 'login' AND !empty($_POST)){
    $check = $obj->checkUser($_POST['user'], $_POST['password']);
    if($check){
        die("1");
    }else{
        die("2");
    }
}elseif ($_POST['type'] == 'formulario' AND !empty($_POST)) {
    $check = $obj->newRecord($_POST['descripcion'], $_POST['sede'], $_POST['presupuesto'], $_POST['dolares']);
    if($check){
        die("1");
    }else{
        die("2");
    }
}elseif($_POST['type'] == 'lista' AND !empty($_POST)) {
    $ArrayRecords = $obj->getRecordByDate($_POST['fecha']);
    if(!empty($ArrayRecords)){
        $table = "<table class='table table-bordered'>";
        $table .= "<thead>";
        $table .= "<tr>";
        $table .= "<th>ID</th>";
        $table .= "<th>DESCRIPCION</th>";
        $table .= "<th>SEDE</th>";
        $table .= "<th>EDITAR</th>";
        $table .= "<th>VER</th>";
        $table .= "</tr>";
        $table .= "</thead>";
        $table .= "<tbody>";
        foreach ($ArrayRecords as $key) {
            $table .= "<tr>";
            foreach ($key as $key2 => $value) {
                $table .= "<td>".$value."</td>";
            }
            $table .= "</tr>";
        }
        $table .= "</tbody>";
        $table .= "</table>";
        echo $table;
    }else{
        echo "<h1>No hay registros</h1>";
    }
}elseif($_POST['type'] == 'editarRegistro' AND !empty($_POST)) {
    $obj->updateRecord($_POST['descripcion'], $_POST['sede'], $_POST['presupuesto'], $_POST['dolares'], $_POST['id']);
    header("Location: lista.php");
}
?>