<?php
include "header.php";
include "clases.php";
$obj = new Metodos();
$ArrayRecord = $obj->getRecordById($_GET['id']);
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-default" href="login.php">Cerrar Sesión</a>
            <a href="lista.php" class="btn btn-default">Registros</a>
        </div>
    </div>
    <div class="row">
        <form action="async_method.php" method="POST" class="col-md-12" id="formFormulario">
            <div class="form-group">
                <label for="id">Número de proceso:</label>
                <input type="text" value="<?php echo str_pad(($ArrayRecord[0]+1), 8, "0", STR_PAD_LEFT); ?>" class="form-control" readonly="readonly">
                <input type="hidden" name="id" value="<?php echo $ArrayRecord[0];?>">
            </div>
            <div class="form-group">
                <label for="descripcion">Descripción:</label>
                <input type="text" required="required" name="descripcion" value="<?php echo $ArrayRecord[1];?>" id="descripcion" class="form-control">
            </div>
            <div class="form-group">
                <label for="fecha">Fecha de creación:</label>
                <input type="text" name="fecha" id="fecha" value="<?php echo $ArrayRecord[2];?>" readonly="readonly" class="form-control">
            </div>
            <div class="form-group">
                <label for="sede">Sede:</label>
                <?php echo $ArrayRecord[3];?>
            </div>
            <div class="form-group">
                <label for="presupuesto">Presupuesto:</label>
                <input type="number" required="required" value="<?php echo $ArrayRecord[4];?>" min="0" name="presupuesto" id="presupuesto" placeholder="COP (Pesos Colombianos)" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" name="dolares" id="dolares" value="<?php echo $ArrayRecord[5];?>" readonly="readonly" placeholder="USD (Dólar Americano)" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" value="Guardar" class="btn btn-primary">
            </div>
            <input type="hidden" name="type" value="editarRegistro">
        </form>
    </div>
</div>
<?php include "footer.php"; ?>