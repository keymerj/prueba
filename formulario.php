<?php
include "header.php";
include "clases.php";
$obj = new Metodos();
$nProccess = $obj->idProccess();

if(empty($nProccess['id'])){
    $id2 = '00000001';
}else{
    $id2 = str_pad(($nProccess['id']+1), 8, "0", STR_PAD_LEFT);
}

$ArraySede = $obj->obtenerSedes();
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-default" href="login.php">Cerrar Sesión</a>
            <a href="lista.php" class="btn btn-default">Registros</a>
        </div>
    </div>
    <div class="row">
        <form action="" method="POST" class="col-md-12" id="formFormulario">
            <div class="form-group">
                <label for="id">Número de proceso:</label>
                <input type="text" name="id" id="id" value="<?php echo $id2; ?>" class="form-control" readonly="readonly">
            </div>
            <div class="form-group">
                <label for="descripcion">Descripción:</label>
                <input type="text" name="descripcion" id="descripcion" class="form-control">
            </div>
            <div class="form-group">
                <label for="fecha">Fecha de creación:</label>
                <input type="text" name="fecha" id="fecha" value="<?php echo date('Y-m-d');?>" readonly="readonly" class="form-control">
            </div>
            <div class="form-group">
                <label for="sede">Sede:</label>
                <select name="sede" id="sede" class="form-control">
                <?php foreach($ArraySede as $key){?>
                <option value="<?php echo $key['id'];?>"><?php echo utf8_encode($key['sede']);?></option>
                <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="presupuesto">Presupuesto;</label>
                <input type="number" min="0" name="presupuesto" id="presupuesto" placeholder="COP (Pesos Colombianos)" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" name="dolares" id="dolares" readonly="readonly" placeholder="USD (Dólar Americano)" class="form-control">
            </div>
            <div class="form-group">
                <a class="btn btn-primary" onclick="envio_datos('formFormulario');">Guardar</a>
            </div>
            <div class="alert alert-success" role="alert">Se guardó con éxito</div>
            <div class="alert alert-warning" role="alert">No se guardó</div>
            <div class="alert alert-danger" role="alert">Campos vacíos</div>
            <input type="hidden" name="type" value="formulario">
        </form>
    </div>
</div>
<?php include "footer.php"; ?>