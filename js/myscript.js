$('document').ready(function(){
    $('body').on('keypress','#presupuesto',function(){
        var monto = $(this).val();
        var montoDolar = parseFloat(monto+'0.00') / parseFloat('2750.00');
        $('#dolares').val(montoDolar.toFixed(2)+' $');
    });

    $('#presupuesto').focusout(function(){
        $('#dolares').val('');
        var monto = $('#presupuesto').val();
        var montoDolar = parseFloat(monto+'.00') / parseFloat('2750.00');
        $('#dolares').val(montoDolar.toFixed(2)+' $');
    });

    $(function(){
        $('body').on('focus','#fecha', function(){
          $(this).datepicker({
              changeMonth: true,
              changeYear: true,
              dateFormat: "yy-mm-dd"
          });
        });
    });
});

function envio_datos(form){
    var a = 0;
    $('form#'+form+' .form-control').each(function(){
        if($(this).val() == ''){
            a++;
        }
    });
    if(a == 0 || form == 'formRecords'){
        $.ajax({
            type: 'POST',
            url: 'async_method.php',
            data: $("form#"+form).serialize(),
            success: function(data) {
                console.log(data);
                if(form == 'formLogin'){
                    if(data == '1'){
                        window.location.href = 'formulario.php';
                    }else{
                        $('form#'+form+' .alert.alert-danger').show(500).fadeOut(3000);
                    }
                }else if(form == 'formFormulario'){
                    if(data == '1'){
                        $('form#'+form+' .alert.alert-success').show(500).fadeOut(3000);
                        clearFields(form);
                    }else{
                        $('form#'+form+' .alert.alert-warning').show(500).fadeOut(3000);
                    }
                }else if(form == 'formRecords'){
                    $('div#tableRecord').empty();
                    $('div#tableRecord').append(data);
                }
            }
        });
    }else{
        $('form#'+form+' .alert.alert-danger').show(500).fadeOut(3000);
    }
}

function clearFields(form){
    $('form#'+form+' .form-control').val('');
}