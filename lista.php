<?php
include "header.php";
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-right">
            <a class="btn btn-default" href="login.php">Cerrar Sesión</a>
            <a href="formulario.php" class="btn btn-default">Registrar</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <h4>Datos Guardados</h4>
        </div>
        <form method="POST" class="col-md-12" id="formRecords">
            <div class="form-group col-md-6">
                <input type="text" name="fecha" id="fecha" placeholder="Fecha a buscar" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <a onclick="envio_datos('formRecords');" class="btn btn-primary">Buscar</a>
            </div>
            <input type="hidden" name="type" value="lista">
        </form>
        <div class="col-md-12" id="tableRecord">
            
        </div>
    </div>
</div>
<?php
include "footer.php";
?>