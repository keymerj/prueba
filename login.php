<?php include "header.php"; ?>
   <div class="container">
       <div class="row">
           <form action="" method="POST" class="col-md-12" id="formLogin">
               <div class="form-group">
                   <label for="user">Usuario:</label>
                   <input type="text" name="user" id="user" class="form-control">
               </div>
               <div class="form-group">
                   <label for="password">Contraseña:</label>
                   <input type="password" name="password" id="password" class="form-control">
               </div>
               <div class="form-group">
                   <a class="btn btn-primary" onclick="envio_datos('formLogin');">ENTRAR</a>
               </div>
               <div class="alert alert-danger" role="alert">Error al iniciar</div>
               <input type="hidden" name="type" value="login">
           </form>
       </div>
   </div>
<?php include "footer.php"; ?>