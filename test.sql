/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-10-19 07:03:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for formulario
-- ----------------------------
DROP TABLE IF EXISTS `formulario`;
CREATE TABLE `formulario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` blob,
  `fecha_creacion` date DEFAULT NULL,
  `sede` int(11) DEFAULT NULL,
  `presupuesto` float(11,2) DEFAULT NULL,
  `dolares` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of formulario
-- ----------------------------
INSERT INTO `formulario` VALUES ('1', 0x6D6F6469666963616E646F2E2E2E, '2018-10-18', '2', '135000.00', '49.09 $');
INSERT INTO `formulario` VALUES ('2', 0x7A7A7A7A, '2018-10-18', '1', '135000.00', '49.09 $');
INSERT INTO `formulario` VALUES ('3', 0x7A7A7A7A, '2018-10-18', '1', '135000.00', '49.09 $');
INSERT INTO `formulario` VALUES ('4', 0x536567756E6461207072756562612E2E2E2032C2B0, '2018-10-18', '1', '1200000.00', '436.36 $');
INSERT INTO `formulario` VALUES ('5', 0x7A7A7A7A, '2018-10-18', '1', '135000.00', '49.09 $');
INSERT INTO `formulario` VALUES ('6', 0x6465736372697063696F6E20707275656261, '2018-10-18', '1', '239000.00', '86.91 $');

-- ----------------------------
-- Table structure for sede
-- ----------------------------
DROP TABLE IF EXISTS `sede`;
CREATE TABLE `sede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sede` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sede
-- ----------------------------
INSERT INTO `sede` VALUES ('1', 'Bogotá');
INSERT INTO `sede` VALUES ('2', 'México');
INSERT INTO `sede` VALUES ('3', 'Perú');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '1234');
